create table student(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  email varchar(255),
  age int,
  phone varchar(255),
  PRIMARY KEY(id)
);


create table teacher(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    age int,
    phone varchar(255),
    PRIMARY KEY (id)
);

create table course(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  code varchar(255) NOT NULL UNIQUE,
  PRIMARY KEY(id)
);

create table section(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255),
  teacher_id int NOT NULL,
  course_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY(teacher_id) REFERENCES teacher(id),
  FOREIGN KEY(course_id) REFERENCES course(id),
  CONSTRAINT unique_section UNIQUE (teacher_id, course_id)
);

create table enrollment(
  id int NOT NULL AUTO_INCREMENT,
  student_id int NOT NULL,
  section_id int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (student_id) REFERENCES student(id),
  FOREIGN KEY (section_id) REFERENCES section(id)
);
