<?php
//singleton class to manage database connection

class db
{
    private static $instance  = null;
    private $conn;
    private $host = "localhost";
    private $user = "root";
    private $pass = "123";
    private $name = "test_project";

    //constructor is private to allow only one initilization
    private function __construct()
    {
        $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->name);
    }

    //getting current instance of DB. Creates a new if doesn't exist. 
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new db();
        }

        return self::$instance;
    }

    //dynamically inserting data to tables
    public function insert($table, $data)
    {
        //if db connection is not initialized
        if (!self::$instance) {
            die("DB NOT INITIALIZED");
        }

        //query Building--------------------

        //appending keys
        $query = "insert into $table (" . implode(",", array_keys($data)) . ") ";
        //appending values
        $query .= "values ('" . implode("' , '", array_values($data)) . "');";
        //-----------------------------------

        //executing query
        $result  = $this->conn->query($query);

        if (!$result) {
            die("DB QUERY FAILED");
        }
        if ($result) echo ("<script>alert('record added successfully')</script>");
    }

    //to fetch all records from a table 
    public function fetchAll($table)
    {
        $query  = "Select * from $table ;";
        $result = $this->conn->query($query);
        if (!$result) {
            die("db query failed");
        }
        return $result;
    }

    //to single record from table 
    public function fetchOne($table, $id)
    {
        $query  = "Select * from $table where id=$id;";
        $result = $this->conn->query($query);
        if (!$result) {
            die("db query failed");
        }
        return $result;
    }

    //to execute a raw query
    public function executeRaw($query)
    {
        $result = $this->conn->query($query);
        if (!$result) {
            die("db query failed");
        }
        return $result;
    }

    //closing database connection
    public function closeConnection()
    {
        $this->conn->close();
        self::$instance = null;
    }
}
