<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HOME</title>
    <link rel="stylesheet" href="./css//index.css">
</head>

<body>
    <h1>WELCOME</h1>
    <ul class="nav_list">
        <li> <a href="./database_diagram.svg">DATABASE DIAGRAM</a></li>

        <h2>FORMS</h2>
        <li> <a href="./forms/course.php">add course</a></li>
        <li> <a href="./forms/student.php">add student</a></li>
        <li> <a href="./forms/teacher.php">add teacher</a></li>
        <li> <a href="./forms/enrollment.php">add enrollment</a></li>
        <li> <a href="./forms/section.php">add section</a></li>

        <h2>VIEWS</h2>
        <li> <a href="./views/course.php">view courses</a></li>
        <li> <a href="./views/student.php">view students</a></li>
        <li> <a href="./views/teacher.php">view teachers</a></li>
        <li> <a href="./views/enrollment.php">view enrollments</a></li>
        <li> <a href="./views/section.php">view sections</a></li>

    </ul>
</body>

</html>
