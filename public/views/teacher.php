<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>Teachers</title>
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    $instance  = db::getInstance();
    if (isset($_GET['id'])) {
        $teachers  = $instance->fetchOne('teacher', $_GET['id']);
    } else {
        $teachers  = $instance->fetchAll('teacher');
    }
    ?>
    <h1><?php echo $_GET['id'] ?  "Teacher" : "Teachers"; ?></h1>
    <?php
    if (!isset($_GET['id'])) {
        echo "<a class='button' href='../forms/teacher.php'>New Teacher</a>";
    }
    ?>
    <table>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>email</th>
            <th>age</th>
            <th>phone</th>
        </tr>
        <?php
        while ($row = $teachers->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['name'] . "</td>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['age'] . "</td>";
            echo "<td>" . $row['phone'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>

</html>