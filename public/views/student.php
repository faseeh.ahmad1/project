<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>Students</title>
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    $instance  = db::getInstance();
    if (isset($_GET['id'])) {
        $students  = $instance->fetchOne('student', $_GET['id']);
    } else {
        $students  = $instance->fetchAll('student');
    }
    ?>
    <h1><?php echo $_GET['id'] ?  "Student" : "Students"; ?></h1>
    <?php
    if (!isset($_GET['id'])) {
        echo "<a class='button' href='../forms/student.php'>New Student</a>";
    }
    ?>

    <table>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>email</th>
            <th>age</th>
            <th>phone</th>
        </tr>
        <?php
        while ($row = $students->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['name'] . "</td>";
            echo "<td>" . $row['email'] . "</td>";
            echo "<td>" . $row['age'] . "</td>";
            echo "<td>" . $row['phone'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>

</html>