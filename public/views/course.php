<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>Courses List</title>
</head>
<body>
    <?php
        require_once("../../includes/db.php");
        $instance  = db::getInstance();
        if(isset($_GET['id'])){
            $courses  = $instance->fetchOne('course',$_GET['id']);
        }
        else{
            $courses  = $instance->fetchAll('course');
        }
    ?>
    <h1><?php echo $_GET['id'] ?  "Course" : "Courses"; ?></h1>
    <?php
            if(!isset($_GET['id'])){
                echo "<a class='button' href='../forms/course.php'>New Course</a>";
            }
        ?> 
    <table>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>Course Code</th>
        </tr>
        <?php
        while($row = $courses->fetch_assoc()){
            echo "<tr>";
            echo "<td>".$row['id']."</td>";
            echo "<td>".$row['name']."</td>";
            echo "<td>".$row['code']."</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>
</html>