<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>Courses List</title>
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    $instance  = db::getInstance();

    $query = "select section.teacher_id,section.course_id,section.id,section.name,teacher.name as teacher,course.name as course
                from section
                INNER JOIN  teacher ON teacher.id = section.teacher_id";
    if (isset($_GET['id'])) //conditional concatenation if id exist
        $query .= " AND section.id='" . $_GET['id'] . "'";
    $query .= " INNER JOIN  course ON course.id = section.course_id;";

    $sections  = $instance->executeRaw($query);
    ?>
    <h1><?php echo $_GET['id'] ?  "Section" : "Sections"; ?></h1>
    <?php
    if (!isset($_GET['id'])) {
        echo "<a class='button' href='../forms/section.php'>New Section</a>";
    }
    ?>
    <table>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>teacher</th>
            <th>course</th>
        </tr>
        <?php
        while ($row = $sections->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['name'] . "</td>";
            echo "<td><a href='teacher.php?id=" . $row['teacher_id'] . "'>" . $row['teacher'] . "</a></td>";
            echo "<td><a href='course.php?id=" . $row['course_id'] . "'>" . $row['course'] . "</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>

</html>