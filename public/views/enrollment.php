<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>Courses List</title>
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    $instance  = db::getInstance();
    $query = " select enrollment.student_id,enrollment.section_id,enrollment.id,student.name as student,section.name as section
                from enrollment
                INNER JOIN  student ON student.id=enrollment.student_id";
    if (isset($_GET['id'])) //conditional concatenation if id exist
        $query .= " AND enrollment.id='" . $_GET['id'] . "'";
    $query .= " INNER JOIN  section ON section.id=enrollment.section_id;";

    $enrollments  = $instance->executeRaw($query);
    ?>

    <h1><?php echo $_GET['id'] ?  "Student Enrollment" : "Student Enrollments"; ?></h1>
    <?php
    if (!isset($_GET['id'])) {
        echo "<a class='button' href='../forms/enrollment.php'>Enroll new Student</a>";
    }
    ?>
    <table>
        <tr>
            <th>id</th>
            <th>Student</th>
            <th>Section</th>
        </tr>
        <?php
        while ($row = $enrollments->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td><a href='student.php?id=" . $row['student_id'] . "'>" . $row['student'] . "</a></td>";
            echo "<td><a href='section.php?id=" . $row['section_id'] . "'>" . $row['section'] . "</a></td>";

            echo "</tr>";
        }
        ?>
    </table>
</body>

</html>