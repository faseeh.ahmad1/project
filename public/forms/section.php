<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>section</title>
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    $instance  = db::getInstance();
    $courses  = $instance->fetchAll('course');
    $teachers = $instance->fetchAll('teacher');
    if (isset($_POST['submit'])) {
        //removing submit element(if exist in post array)
        if ($_POST['submit']) {
            $_POST = array_slice($_POST, 0, sizeof($_POST) - 1);
        }
        //query execution
        $instance->insert('section', $_POST);
        //redirection
        header("Location: ../views/section.php");
        exit();
    }
    ?>
    <div class="container">
        <form method='POST' class="form">
            <h2 class="form_heading">Add a section</h2>
            <label for="name">Section Name</label>
            <input class="input" type="text" name="name" required>

            <label for='course'>Select course</label>
            <select class='input' name='course_id' required>
                <?php
                while ($row  = $courses->fetch_assoc()) {
                    echo "<option value=" . $row['id'] . ">" . $row['name'] . "(" . $row['code'] . ")</option>";
                    echo "<br/>";
                }
                ?>
            </select>

            <label for='teacher'>Select teacher</label>
            <select class='input' name='teacher_id' required>
                <?php
                while ($row  = $teachers->fetch_assoc()) {
                    echo "<option value=" . $row['id'] . ">" . $row['name'] . "</option>";
                    echo "<br/>";
                }
                ?>
            </select>

            <input class="submit" name="submit" type="submit" />

        </form>
    </div>
</body>

</html>