<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>new course</title>
    <link rel="stylesheet" href="../css/index.css">
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    if (isset($_POST['submit'])) {
        $instance  = db::getInstance();
        //removing submit element(if exist in post array)
        if ($_POST['submit']) {
            $_POST = array_slice($_POST, 0, sizeof($_POST) - 1);
        }
        //query execution
        $instance->insert('course', $_POST);
        //redirection
        header("Location: ../views/course.php");
        exit();
    }
    ?>
    <div class="container">
        <form method='POST' class="form">
            <h2 class="form_heading">Create new Course</h2>
            <label for='name'>Course Name</label>
            <input type='text' class='input' name='name' required>
            <label for='code'>Course Code</label>
            <input type='text' class='input' name='code' required>

            <input class='submit' name='submit' type='submit'>
        </form>
    </div>
</body>


</html>