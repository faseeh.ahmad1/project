<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/index.css">
    <title>Enrollment</title>
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    $instance  = db::getInstance();
    $students  = $instance->fetchAll('student');
    $sections = $instance->fetchAll('section');
    if (isset($_POST['submit'])) {
        //removing submit element(if exist in post array)
        if ($_POST['submit']) {
            $_POST = array_slice($_POST, 0, sizeof($_POST) - 1);
        }
        //query execution
        $instance->insert('enrollment', $_POST);
        header("Location: ../views/enrollment.php");
        exit();
    }
    ?>
    <div class="container">
        <form method='POST' class="form">
            <h2 class="form_heading">Enroll Student</h2>
            <label for='course'>Select Student</label>
            <select class='input' name='student_id' required>
                <?php
                while ($row  = $students->fetch_assoc()) {
                    echo "<option value=" . $row['id'] . ">" . $row['name'] ."</option>";
                    echo "<br/>";
                }
                ?>
            </select>

            <label for='teacher'>Select Section</label>
            <select class='input' name='section_id' required>
                <?php
                while ($row  = $sections->fetch_assoc()) {
                    echo "<option value=" . $row['id'] . ">" . $row['name'] . "</option>";
                    echo "<br/>";
                }
                ?>
            </select>

            <input class="submit" name="submit" type="submit" />

        </form>
    </div>
</body>

</html>