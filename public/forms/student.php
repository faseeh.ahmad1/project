<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>new student</title>
    <link rel="stylesheet" href="../css/index.css">
</head>

<body>
    <?php
    require_once("../../includes/db.php");
    if (isset($_POST['submit'])) {
        $instance  = db::getInstance();
        //removing submit element(if exist in post array)
        if ($_POST['submit']) {
            $_POST = array_slice($_POST, 0, sizeof($_POST) - 1);
        }
        //query execution
        $instance->insert('student', $_POST);
        //redirection
        header("Location: ../views/student.php");
        exit();
    }
    ?>
    <div class="container">
        <form method='POST' class="form">
            <h2 class="form_heading">Create new Student</h2>
            <label for='name'>Student's Name</label>
            <input type='text' class='input' name='name' required>
            <label for='email'>Email</label>
            <input type='email' class='input' name='email' required>
            <label for='age'>Age</label>
            <input type='number' min=1 class='input' name='age' required>
            <label for='email'>Phone</label>
            <input type='text' class='input' name='phone' required>
            <input class='submit' name='submit' type='submit'>
        </form>
    </div>
</body>

</html>